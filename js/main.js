var danhSachSinhVien = [];

var validatorSv = new ValidatorSV();

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const timKiemViTri = function (id, array) {
  return array.findIndex(function (sv) {
    return sv.maSv == id;
  });
};

const luuLocalStorage = function () {
  var dssvJson = JSON.stringify(danhSachSinhVien);

  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};

// lấy dữ liệu từ localStorage khi user tải lại trang

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);

//  gán cho array gốc và render lại giao diện
if (dssvJson) {
  danhSachSinhVien = JSON.parse(dssvJson);
  xuatDanhSachSinhVien(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();

  var isValid = true;

  var index = danhSachSinhVien.findIndex(function (item) {
    return item.maSv == newSinhVien.maSv;
  });

  validatorSv.kiemTraRong(
    "txtEmail",
    "spanEmailSV",
    "Email không được để rỗng"
  );

  // console.log(index);
  if (index == -1) {
    danhSachSinhVien.push(newSinhVien);
    xuatDanhSachSinhVien(danhSachSinhVien);
    document.getElementById("spanMaSV").innerText = "";
    // convert array thành json để có thể lưu vào localStorage

    luuLocalStorage();
    // console.log(dssvJson);
  } else {
    document.getElementById("spanMaSV").innerText =
      "Mã sinh viên không được trùng";
  }

  //   console.log(danhSachSinhVien);
}

function xoaSinhVien(id) {
  // console.log(id);
  var viTri = timKiemViTri(id, danhSachSinhVien);
  // console.log({ viTri });
  // xoá tại ví trị tìm thấy với số lượng là 1
  danhSachSinhVien.splice(viTri, 1);
  xuatDanhSachSinhVien(danhSachSinhVien);
  luuLocalStorage();
}

function suaSinhVien(id) {
  var viTri = timKiemViTri(id, danhSachSinhVien);
  console.log({ viTri });

  var sinhVien = danhSachSinhVien[viTri];

  xuatThongTinLenForm(sinhVien);
}
function capNhatSinhVien() {
  var sinhVienEdit = layThongTinTuForm();
  console.log({ sinhVienEdit });

  let viTri = timKiemViTri(sinhVienEdit.maSv, danhSachSinhVien);

  danhSachSinhVien[viTri] = sinhVienEdit;

  xuatDanhSachSinhVien(danhSachSinhVien);

  luuLocalStorage();
}
